package test;

import org.junit.jupiter.api.Test;
import src.NumerosReales;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumerosRealesTest {

    @Test
    public void testPositivoParMayorQue10() {
        int numero = 10;
        String expectedOutput = "El número es par\r\n El número es par y mayor que 10\r\nEl número es par y menor o igual que 10\r\n";
        assertEquals(expectedOutput, getOutputFromMain(numero));
    }

    @Test
    public void testPositivoParMenorIgualQue10() {
        int numero = 8;
        String expectedOutput = "El número es par\r\nEl número es par y menor o igual que 10\r\n";
        assertEquals(expectedOutput, getOutputFromMain(numero));
    }

    @Test
    public void testImparNegativo() {
        int numero = -3;
        String expectedOutput = "El número es impar\r\nEl número es impar y negativo\r\nEl número es impar y positivo\r\n";
        assertEquals(expectedOutput, getOutputFromMain(numero));
    }

    @Test
    public void testImparPositivo() {
        int numero = 7;
        String expectedOutput = "El número es impar\r\nEl número es impar y positivo\r\n";
        assertEquals(expectedOutput, getOutputFromMain(numero));
    }

    @Test
    public void testNegativo() {
        int numero = -5;
        String expectedOutput = "El número es negativo\r\n";
        assertEquals(expectedOutput, getOutputFromMain(numero));
    }

    @Test
    public void testCero() {
        int numero = 0;
        String expectedOutput = "El número es negativo\r\n";
        assertEquals(expectedOutput, getOutputFromMain(numero));
    }


    @Test
    public void testNumeroCinco() {
        int numero = 5;
        String expectedOutput = "El número es impar\r\nEl número es impar y positivo\r\n";
        assertThat(getOutputFromMain(numero), containsString(expectedOutput));
    }


    // Método para obtener la salida del programa según la entrada
    private String getOutputFromMain(int numero) {
        // Redirige la salida estándar para capturarla en un string
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));

        // Llama a la función main con el número dado
        NumerosReales.main(new String[]{String.valueOf(numero)});

        // Restaura la salida estándar
        System.setOut(originalOut);

        // Devuelve la salida capturada como un string
        return outputStream.toString();
    }
}
